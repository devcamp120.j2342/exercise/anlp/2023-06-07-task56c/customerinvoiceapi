package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;

    public ArrayList<Invoice> getAllInvoice(){
        Invoice invoice1 = new Invoice(1, customerService.customer1, 10000);
        Invoice invoice2 = new Invoice(2, customerService.customer2, 10000);
        Invoice invoice3 = new Invoice(3, customerService.customer3, 7000);
    
        ArrayList<Invoice> allInvoice = new ArrayList<>();
        allInvoice.add(invoice1);
        allInvoice.add(invoice2);
        allInvoice.add(invoice3);

        return allInvoice;
    }
}
